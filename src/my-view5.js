/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

import './shared-styles.js';

class MyView5 extends PolymerElement {
  static get template() {
    return html`
   
    <style include="shared-styles">
    :host {
      display: block;

     
    }

  </style>

 

  <div class="card" style="width: 100%; margin:0px auto;">
    <img class="card-img-top" src="../images/beautiful.jpg" alt="Card image cap">
    <div class="card-body">

      
      <div class="input-group input-group-sm mb-3" style="width:40%;margin:0px auto;">
      <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-sm">Username</span>
      </div>
      <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
    </div>

    <div class="input-group input-group-sm mb-3" style="width:40%; margin:0px auto;">
      <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-sm">Password</span>
      </div>
      <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
    </div>

      <a href="#" class="btn btn-primary "style="float:right;">Sign In</a>
    </div>
  </div>

    `;
  }
}

window.customElements.define('my-view5', MyView5);
